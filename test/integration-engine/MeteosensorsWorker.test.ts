import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { config } from "@golemio/core/dist/integration-engine/config";
import { DataSourceStream } from "@golemio/core/dist/integration-engine/datasources/DataSourceStream";
import { Meteosensors } from "#sch/index";
import { MeteosensorsWorker } from "#ie/MeteosensorsWorker";

const waitTillStreamEnds = async (stream: any) => {
    await new Promise((resolve) => {
        const checker = setInterval(async () => {
            // con not use on.end handler directly
            if (stream.destroyed) {
                clearInterval(checker);
                resolve(null);
            }
        }, 100);
    });
};

describe("MeteosensorsWorker", () => {
    let worker: MeteosensorsWorker;
    let sandbox: SinonSandbox;
    let queuePrefix: string;
    let testData: any[];
    let testTransformedData: any[];
    let testTransformedHistoryData: any[];
    let data0: Record<string, any>;
    let data1: Record<string, any>;
    let dataStream: DataSourceStream;

    beforeEach(() => {
        sandbox = sinon.createSandbox();

        testData = [
            {
                airTemperature: 12.7,
                alarm: 0,
                devPoint: 5.29,
                district: null,
                freezTemperature: 0,
                gid: null,
                humidity: 60.79,
                id: 531801,
                lastUpdated: new Date().getTime(),
                lat: 50.03993,
                lng: 14.405042,
                mark: null,
                name: "1-Barrandov",
                prec: 0,
                prectype: 0,
                prefix: null,
                roadTemperature: 23,
                roadWet: null,
                sgid: 1,
                spray: null,
                sprayProgram: 0,
                street: null,
                tankLevel: 61.23,
                technology: "Boschung",
                tmsAlarm: 0,
                windDirection: 185,
                windSpeed: 1.49,
            },
            {
                airTemperature: 11.4,
                alarm: 0,
                devPoint: 4.05,
                district: null,
                freezTemperature: 0,
                gid: null,
                humidity: 60.69,
                id: 531802,
                lastUpdated: new Date().getTime(),
                lat: 50.085625,
                lng: 14.436838,
                mark: null,
                name: "2-Bulhar",
                prec: 0,
                prectype: 0,
                prefix: null,
                roadTemperature: 27.05,
                roadWet: null,
                sgid: 1,
                spray: null,
                sprayProgram: 0,
                street: null,
                tankLevel: 54.59,
                technology: "Boschung",
                tmsAlarm: 0,
                windDirection: 40,
                windSpeed: 1.78,
            },
        ];
        testTransformedData = [
            {
                geometry: {
                    coordinates: [14.405042, 50.03993],
                    type: "Point",
                },
                properties: {
                    air_temperature: 13.01,
                    humidity: 57,
                    id: 531801,
                    last_updated: 1589448004000,
                    name: "Barrandov",
                    road_temperature: 23.32,
                    updated_at: 1589448024805,
                    wind_direction: null,
                    wind_speed: 1.17,
                },
                type: "Feature",
            },
            {
                geometry: {
                    coordinates: [14.436838, 50.085625],
                    type: "Point",
                },
                properties: {
                    air_temperature: 10.94,
                    humidity: 61,
                    id: 531802,
                    last_updated: 1589448009000,
                    name: "Bulhar",
                    road_temperature: 25.37,
                    updated_at: 1589448024805,
                    wind_direction: 95,
                    wind_speed: 1.05,
                },
                type: "Feature",
            },
        ];
        testTransformedHistoryData = [1, 2];
        data0 = { properties: { id: 0 }, geometry: { coordinates: [0, 0] } };
        data1 = { properties: { id: 1 }, geometry: { coordinates: [1, 1] }, save: sandbox.stub().resolves(true) };

        worker = new MeteosensorsWorker();

        const getOutputStream = async (data: any, stream: DataSourceStream) => {
            stream.push(data);
            stream.push(null);
            return stream;
        };

        dataStream = new DataSourceStream({
            objectMode: true,
            read: () => {
                return;
            },
        });

        sandbox.stub(worker["dataSource"], "getOutputStream" as any).callsFake(() => getOutputStream(testData, dataStream));
        sandbox.stub(worker["transformation"], "transform").callsFake(() => Promise.resolve(testTransformedData));
        sandbox.stub(worker["transformation"], "transformHistory").callsFake(() => Promise.resolve(testTransformedHistoryData));
        sandbox.stub(worker["model"], "save");
        sandbox.stub(worker["historyModel"], "save");
        sandbox.stub(worker, "sendMessageToExchange" as any);
        queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + Meteosensors.name.toLowerCase();
        sandbox.stub(worker["model"], "findOneById").callsFake(() => Promise.resolve(data1));

        sandbox.stub(worker["cityDistrictsModel"], "findOne");
        sandbox.spy(worker["dataSource"], "getAll");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should calls the correct methods by refreshDataInDB method", async () => {
        await worker.refreshDataInDB({});
        await waitTillStreamEnds(worker["dataStream"]);

        sandbox.assert.calledOnce(worker["dataSource"].getAll as SinonSpy);
        sandbox.assert.calledOnce(worker["transformation"].transform as SinonSpy);
        sandbox.assert.calledWith(worker["transformation"].transform as SinonSpy, testData);
        sandbox.assert.calledOnce(worker["model"].save as SinonSpy);
        sandbox.assert.calledWith(worker["model"].save as SinonSpy, testTransformedData);
        sandbox.assert.calledThrice(worker["sendMessageToExchange"] as SinonSpy);
        sandbox.assert.calledWith(
            worker["sendMessageToExchange"] as SinonSpy,
            "workers." + queuePrefix + ".saveDataToHistory",
            JSON.stringify(testTransformedData)
        );
        testTransformedData.map((f) => {
            sandbox.assert.calledWith(
                worker["sendMessageToExchange"] as SinonSpy,
                "workers." + queuePrefix + ".updateDistrict",
                JSON.stringify(f)
            );
        });
        sandbox.assert.callOrder(
            worker["dataSource"].getAll as SinonSpy,
            worker["transformation"].transform as SinonSpy,
            worker["model"].save as SinonSpy,
            worker["sendMessageToExchange"] as SinonSpy
        );
    });

    it("should calls the correct methods by saveDataToHistory method", async () => {
        await worker.saveDataToHistory({ content: Buffer.from(JSON.stringify(testTransformedData)) });
        sandbox.assert.calledOnce(worker["transformation"].transformHistory as SinonSpy);
        sandbox.assert.calledWith(worker["transformation"].transformHistory as SinonSpy, testTransformedData);
        sandbox.assert.calledOnce(worker["historyModel"].save as SinonSpy);
        sandbox.assert.calledWith(worker["historyModel"].save as SinonSpy, testTransformedHistoryData);
        sandbox.assert.callOrder(worker["transformation"].transformHistory as SinonSpy, worker["historyModel"].save as SinonSpy);
    });

    it("should calls the correct methods by updateDistrict method (different geo)", async () => {
        await worker.updateDistrict({ content: Buffer.from(JSON.stringify(data0)) });
        sandbox.assert.calledOnce(worker["model"].findOneById as SinonSpy);
        sandbox.assert.calledWith(worker["model"].findOneById as SinonSpy, data0.properties.id);

        sandbox.assert.calledOnce(worker["cityDistrictsModel"].findOne as SinonSpy);
        sandbox.assert.calledOnce(data1.save);
    });

    it("should calls the correct methods by updateDistrict method (same geo)", async () => {
        data1 = {
            geometry: { coordinates: [0, 0] },
            properties: {
                address: "a",
                district: "praha-0",
                id: 1,
            },
            save: sandbox.stub().resolves(true),
        };
        await worker.updateDistrict({ content: Buffer.from(JSON.stringify(data0)) });
        sandbox.assert.calledOnce(worker["model"].findOneById as SinonSpy);
        sandbox.assert.calledWith(worker["model"].findOneById as SinonSpy, data0.properties.id);

        sandbox.assert.notCalled(worker["cityDistrictsModel"].findOne as SinonSpy);
        sandbox.assert.notCalled(data1.save);
    });
});
