import { SchemaDefinition } from "@golemio/core/dist/shared/mongoose";

// MSO = Mongoose SchemaObject

const datasourceMSO: SchemaDefinition = {
    airTemperature: { type: Number },
    alarm: { type: Number },
    devPoint: { type: Number },
    district: { type: String },
    freezTemperature: { type: Number },
    gid: { type: Number },
    humidity: { type: Number },
    id: { type: Number, required: true },
    lastUpdated: { type: Number, required: true },
    lat: { type: Number, required: true },
    lng: { type: Number, required: true },
    mark: { type: Number },
    name: { type: String, required: true },
    prec: { type: Number },
    prectype: { type: Number },
    prefix: { type: String },
    roadTemperature: { type: Number },
    roadWet: { type: Number },
    sgid: { type: Number },
    spray: { type: Number },
    sprayProgram: { type: Number },
    street: { type: Number },
    tankLevel: { type: Number },
    technology: { type: String },
    tmsAlarm: { type: Number },
    windDirection: { type: Number },
    windSpeed: { type: Number },
};

const outputMSO: SchemaDefinition = {
    geometry: {
        coordinates: { type: Array, required: true },
        type: { type: String, required: true },
    },
    properties: {
        air_temperature: { type: Number },
        district: { type: String },
        humidity: { type: Number },
        id: { type: Number, required: true },
        last_updated: { type: Number, required: true },
        name: { type: String, required: true },
        road_temperature: { type: Number },
        updated_at: { type: Number, required: true },
        wind_direction: { type: Number },
        wind_speed: { type: Number },
    },
    type: { type: String, required: true },
};

const outputHistoryMSO: SchemaDefinition = {
    air_temperature: { type: Number },
    humidity: { type: Number },
    id: { type: String, required: true },
    last_updated: { type: Number, required: true },
    road_temperature: { type: Number },
    updated_at: { type: Number, required: true },
    wind_direction: { type: Number },
    wind_speed: { type: Number },
};

const forExport = {
    datasourceMongooseSchemaObject: datasourceMSO,
    history: {
        mongoCollectionName: "meteosensors_history",
        name: "MeteosensorsHistory",
        outputMongooseSchemaObject: outputHistoryMSO,
    },
    mongoCollectionName: "meteosensors",
    name: "Meteosensors",
    outputMongooseSchemaObject: outputMSO,
};

export { forExport as Meteosensors };
